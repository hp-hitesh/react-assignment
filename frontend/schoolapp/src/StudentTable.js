import React, { useState, useEffect } from 'react';
import axios from 'axios';

import MaterialTable from 'material-table';
import { withRouter } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';

import StudentForm from './StudentForm';
const columns = [
  { title: 'Name', field: 'name' },
  { title: 'Email', field: 'email', searchable: false },
  { title: 'Gender', field: 'gender', searchable: false },
  { title: 'Class', field: 'class', editable: 'onAdd', searchable: false },
  { title: 'Birthdate', field: 'birthDate', searchable: false },
  { title: 'Address', field: 'address', searchable: false },
  { title: 'ContactNo', field: 'contactNo', type: 'numeric', searchable: false },
  { title: 'Password', field: 'pass'  }

];

const baseUrl = 'http://localhost:7000/student';

const StudentTable = props => {
  const [data, setData, setState] = useState([]);
  const [name, setName] = useState('');
  const [tempData, setTempData] = useState([]);

  const getdata = async () => {
    await axios.get(baseUrl).then(response => {
      setData(response.data);
      setTempData(response.data);
    });
  };

  useEffect(() => {
    getdata();
  }, []);

  return (
    <React.Fragment>
      <MaterialTable
        title="Student Record"
        columns={columns}
        data={data}
        editable={{
          onRowAdd: oldData =>
            new Promise(resolve => {
              setTimeout(async () => {
                resolve();
                
                await axios
                  .post('http://localhost:7000/student', oldData)
                  .then(response => {
                    setData(response.data);
                  });
              }, 600);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise(resolve => {
              setTimeout(async () => {
                resolve();
                
                await axios
                  .patch(
                    `http://localhost:7000/student/${oldData._id}`,
                    newData
                  )
                  .then(response => {
                    setData(response.data);
                  });
              }, 600);
            }),
          onRowDelete: oldData =>
            new Promise(resolve => {
              setTimeout(async () => {
                resolve();
                
                await axios
                  .delete(`http://localhost:7000/student/${oldData._id}`)
                  .then(response => {
                    setData(response.data);
                  });
              }, 600);
            })
        }}
      />
    </React.Fragment>
  );
};

export default withRouter(StudentTable);
