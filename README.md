project configuration 


    1.  make sure you have mongo DB server  in your system   and it run on localhost port number 27017
    2.  for backend I used node.js go to the studentapp folder install node-modules  using terminal

> npm i node-modules  

"dependencies": {
    "cors": "^2.8.5",
    "express": "^4.17.1",
    "mongodb": "^3.6.2",
    "mongoose": "^5.10.4",
    "nodemon": "^2.0.4"
  }

   install all this dependencies using npm  

   to start node server or run backend application run this  command

> nodemon run start 

  3.  for frontend I used react.js go to the schoolapp folder install node-modules  using terminal

> npm i node-modules  

"dependencies": {
    "@material-ui/core": "^4.11.0",
    "axios": "^0.20.0",
    "material-table": "^1.69.0",
    "material-ui": "^0.20.2",
    "react": "^16.13.1",
    "react-dom": "^16.13.1",
    "react-router-dom": "^5.2.0",
    "react-scripts": "3.4.3"
  },
install all this dependencies using npm  

  to start react app or run backend application run this  command

> npm start
